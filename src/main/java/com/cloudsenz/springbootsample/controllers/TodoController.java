package com.cloudsenz.springbootsample.controllers;

import com.cloudsenz.springbootsample.collections.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.cloudsenz.springbootsample.repositories.TodoRepo;

import java.util.List;
import java.util.Optional;

@Controller
public class TodoController {
    @Autowired
    private TodoRepo todoRepo;

    @GetMapping(path = "/")
    public String showTodos(Model model) {
        List<Todo> todos = todoRepo.findAll();
        model.addAttribute("todos", todos);
        model.addAttribute("todo", new Todo());
        return "index";
    }

    @PostMapping(path = "/")
    public String createTodo(@ModelAttribute Todo todo) {
        todoRepo.save(todo);
        return "redirect:/";
    }

    @PostMapping(path = "/{id}")
    public String editTodo(@PathVariable String id, @ModelAttribute Todo todoObj) {
        Optional<Todo> todo = todoRepo.findById(id);

        if(todo.isPresent()) {
            todo.get().setTask(todoObj.getTask());
            todoRepo.save(todo.get());

            return "redirect:/";
        }
        return "404";
    }

    @GetMapping(path = "/update/{id}")
    public String getEditPage(Model model, @PathVariable String id) {
        Optional<Todo> todo = todoRepo.findById(id);

        if(todo.isPresent()) {
            model.addAttribute("todo", todo.get());
            model.addAttribute("todoObj", new Todo());
            return "update";
        }
        return "404";
    }

    @GetMapping(path="/delete/{id}")
    public String deleteTodo(@PathVariable String id) {
        todoRepo.deleteById(id);
        return "redirect:/";
    }
}
